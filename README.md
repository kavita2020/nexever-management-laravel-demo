Project requirements

php (7.4.1)
MySql (5.7.31)
laravel (8.63.0)

Open terminal

- Go to directory in which you want to clone the project.

- Run clone command to download project from git

    git clone https://kavita2020@bitbucket.org/kavita2020/nexever-management-laravel-demo.git

environment setup

- Make a copy of .env.example file and save it as .env file in project root directory. 
- Update following variable values with configuration credentials in .env file.

For Database

    DB_CONNECTION
    DB_HOST
    DB_PORT
    DB_DATABASE
    DB_USERNAME
    DB_PASSWORD

Install project dependencies

- Enter the project directory

- Run following command to install

    composer install

Database setup

- For creating database

    Create database manually by using command line or phpmyadmin of the same name you mentioned in the DB_DATABASE variable .env file.

- To create tables in database.

    php artisan migrate

Apis

- For api authentication 
    I have used passport laravel package. Firstlt install the passport by following below command:-
    
    php artisan passport:install
    
    With above command you will get client_id and client_secret, which will use for the getting access_token. Now to get the access_token hit the following url with required parameter:-
    
    link - http://localhost:8089/oauth/token
    
    endpoint - /oauth/token
    
    method - POST
    
    parameters

        'grant_type' => 'password',
        'client_id' => 'client-id',
        'client_secret' => 'client-secret',
        'username' => 'user@email.com',
        'password' => 'my-password',

    You will get a access_token in the response. After getting the access_token, you can hit other profile and edit api.


- To employee signup

    link - http://localhost:8089/api/register
    
    endpoint - api/register
    
    method - POST
    
    parameters - 
        'first_name' => 'first_name',
        'last_name' => 'last_name',
        'email' => 'test@mail.com',
        'phone' => 'phone',
        'salary' => 'salary',
        'password' => 'password'

    Headers -
        'Accept': application/json


- To employee login

    link - http://localhost:8089/api/login
    
    endpoint - /api/login
    
    method - POST
    
    Headers -
        'Authorization': Bearer access_token
        'Accept': application/json
    
    parameters - 
        'email' => 'test@mail.com',
        'password' => 'password'
    
    This will login employee detail to this access_token's employee.

- Website

    admin login - http://localhost:8089/admin

    company login - http://localhost:8089

- admin Credentials

    email - admin@mail.com

    password - 12345