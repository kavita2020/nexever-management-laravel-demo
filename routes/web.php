<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SecurityController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return view('security/login', ['login' => 'admin_login']);
    })->name('login_page');

    Route::post('/login', [SecurityController::class, 'login'])->name('admin_login');

    Route::get('/logout', [SecurityController::class, 'logout'])->name('admin_logout');
    
});

Route::get('/', function () {
    return view('security/login',['login' => 'company_login']);
})->name('company_login_page');

Route::post('/login', [SecurityController::class, 'login'])->name('company_login');

Route::prefix('company')->group(function () {

    Route::get('/list', [CompanyController::class, 'index'])->name('company');

    Route::post('/store', [CompanyController::class, 'store'])->name('company_store');

    Route::post('/update', [CompanyController::class, 'update'])->name('company_update');

    Route::post('/destroy', [CompanyController::class, 'destroy'])->name('company_destroy');

    Route::get('/show/{id?}', [CompanyController::class, 'show'])->name('company_show');

    Route::get('/logout', [SecurityController::class, 'logout'])->name('company_logout');

});

Route::prefix('employee')->group(function () {

    Route::get('/list', [EmployeeController::class, 'list'])->name('employee_list');

    Route::post('/store', [EmployeeController::class, 'store'])->name('employee_store');

    Route::post('/update', [EmployeeController::class, 'update'])->name('employee_update');

    Route::post('/destroy', [EmployeeController::class, 'destroy'])->name('employee_destroy');

});