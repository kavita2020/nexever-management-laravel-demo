function showModal(button) {
    var thisTr = button.closest('tr');
    var type = button.data('type');
    var name = "";
    var email = "";
    var id = "";
    
    if (type == "edit") {
        name = thisTr.find('.name').text(); 
        email = thisTr.find('.email').text();
        id = button.data('id');
    }
    
    var title = button.data('title');
    var form_url = button.data('url');
    var modal = $('#companyModal');
    modal.find('.modal-body #em_name').val(name);
    modal.find('.modal-body #em_email').val(email);
    modal.find('.modal-body #em_id').val(id);
    modal.find('.modal-title').text(title)
    modal.find('#companyForm').attr('action',form_url);
    modal.find('.modal-body .alert').hide();
    modal.modal('show');
}