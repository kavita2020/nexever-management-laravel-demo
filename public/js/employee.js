function showModal(button) {
    var thisTr = button.closest('tr');
    var type = button.data('type');
    var phone = "";
    var salary = "";
    var email = "";
    var id = "";
    var first_name = "";
    var last_name = "";
    var companyId = button.data('companyid');

    if (type == "edit") {
        first_name = thisTr.find('.name').data('firstname'); 
        last_name = thisTr.find('.name').data('lastname'); 
        email = thisTr.find('.email').text();
        phone = thisTr.find('.phone').text();
        salary = thisTr.find('.salary').text();
        id = button.data('id');
    }

    var title = button.data('title');
    var form_url = button.data('url');
    var modal = $('#employeeModal');
    modal.find('.modal-body #em_first_name').val(first_name);
    modal.find('.modal-body #em_last_name').val(last_name);
    modal.find('.modal-body #em_email').val(email);
    modal.find('.modal-body #em_id').val(id);
    modal.find('.modal-body #em_phone').val(phone);
    modal.find('.modal-body #em_salary').val(salary);
    modal.find('.modal-body #em_company').val(companyId);
    modal.find('.modal-title').text(title)
    modal.find('#employeeForm').attr('action',form_url);
    modal.find('.modal-body .alert').hide();
    modal.modal('show');
}