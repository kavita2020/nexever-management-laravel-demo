$(document).ready(function(){
    $(document).on('click','.pagination a',function(event){
        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var url = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        getData(page);
    });
});

function getData(page) {
    $.ajax({
        url : '?page=' + page,
        type : 'get',
        datatype : 'html',
    }).done(function(data){
        $('.list-body').empty().html(data);
    }).fail(function(jqXHR,ajaxOptions,thrownError){
        alert('No response from server');
    });
}

$(document).on('click', '.addBtn', function() {
    showModal($(this));
});

$(document).on('click', '.editBtn', function() {
    showModal($(this));
});

$(document).on('click', '.saveData', function() {
    var type = $(this).data('model');
    
    let myForm = $('#'+ type +'Form');
    var editUrl = myForm.attr('action');

    var fd = new FormData(myForm[0]);
    var modal = $('#'+ type +'Modal');
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: editUrl,
        type: 'POST',
        data: fd,
        processData: false,
        contentType: false,
        success: function(res) {
            if (res.status == "success") {
                modal.modal('hide');
                showTosterMessage(res.message);

                var page = window.location.href.split('page=')[1];
                getData(page);
            } else {
                showAlertMessage(modal, res.message);
            }
        }
    });

});

function showAlertMessage(element, message) {
    element.find('.modal-body').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert"><span class="error_message">'+message+'</span><button type="button" class="close alert_close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
}

function showTosterMessage(message) {
    toastr.success(message);
}

$(document).on('click', '.deleteBtn', function(){
    var thisUrl = $(this).data('url');
    var id = $(this).data('id');

    if (confirm("Do you want to delete this entry ?")) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: thisUrl,
            data: {'id':id},
            type: 'POST',
            success: function(res) {
                if(res.status == "success"){
                    showTosterMessage(res.message);

                    var page = window.location.href.split('page=')[1];
                    getData(page);
                }
            }
        });
    }
});