<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{ asset('/css/fontawesome-free/all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/icheck-bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/adminlte.min.css') }}">
    </head>

    <body class="hold-transition login-page">

        @yield('content')
        
    </body>

    <footer>
        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('/js/adminlte.min.js') }}"></script>
    </footer>
</html>