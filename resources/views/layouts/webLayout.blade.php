<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>@yield('title')</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{ asset('/css/fontawesome-free/all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/datatables/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/datatables/responsive.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/datatables/buttons.bootstrap4.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/adminlte.min.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="stylesheet" href="{{ asset('/css/all.css') }}">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
        
            @include('common.header')
            @include('common.sidebar')

            <div class="content-wrapper">
                @yield('content')
                @include('modals.company_modal')
                @include('modals.employee_modal')
            </div>

        </div>
    </body>
    <footer>
        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('/js/datatables/buttons.bootstrap4.min.js') }}"></script>        
        <script src="{{ asset('/js/adminlte.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset('js/demo.js') }}"></script>
        <script src="{{ asset('js/common.js') }}"></script>
        
        @yield('scripts')
        <script>
            $(function () {
                $("#example1").DataTable({
                    "responsive": true, 
                    "lengthChange": false, 
                    "autoWidth": false,
                    "pageLength": 5, 
                    "searching": false,
                    "sorting": false,
                    "paging": false,
                    "ordering": false,
                    "info" : false
                });
            });
        </script>
    </footer>
</html>