<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Employee list</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                    <button type="button" class="btn btn-primary btn-small addBtn" data-title="Add Employee" data-url="{{ route('employee_store') }}" data-companyId="{{ $company['id'] }}" data-type="add">Add Employee</button>
                </li>
            </ol>
        </div>
    </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body list-body">
                        @include('employee.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>