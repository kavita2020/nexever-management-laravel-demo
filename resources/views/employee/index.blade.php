@extends('layouts.webLayout')

@section('title', 'Employee Detail')

@section('content')

    @include('employee.data')

@endsection

@section('scripts')
    <script src="{{ asset('/js/employee.js') }}"></script>
@endsection