<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Salary</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="list_body">
        @foreach ($employee as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td class="name" data-firstname="{{ $value->first_name }}" data-lastname="{{ $value->last_name }}"> {{ $value->first_name }} {{ $value->last_name }}</td>
                <td class="email"> {{ $value->email }} </td>
                <td class="phone"> {{ $value->phone }} </td>
                <td class="salary"> {{ $value->salary }} </td>
                <td>
                    <button type="button" class="btn btn-primary mg-left editBtn" data-title="Edit Company" data-url="{{ route('employee_update') }}" data-id="{{ $value->id }}" data-type="edit">Edit</button>
                    <button type="button" class="btn btn-danger mg-left deleteBtn" data-id="{{ $value->id }}" data-url="{{ route('employee_destroy') }}">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="pagination">
    {!! $employee->render() !!}
</div>