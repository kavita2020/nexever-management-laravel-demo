<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="javascript:void(0);" class="brand-link">
    <span class="brand-text font-weight-light">NexEver MANAGEMENT</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="info">
        <a href="javascript:void(0);" class="d-block">
          @if (Auth::guard('company')->user()) 
            <?php 
              $listName = "Employee";
              $route = "employee_list";  
            ?>
            {{ Auth::guard('company')->user()->name }}
          @else
            <?php $listName = "Company";
              $route = "company";  
            ?>
            {{ Auth::guard('admin')->user()->username }}
          @endif
        </a>
      </div>
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="{{ route($route) }}" class="nav-link">
            <p>
              {{ $listName }} List
            </p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>