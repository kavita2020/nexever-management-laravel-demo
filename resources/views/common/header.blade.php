<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav ml-auto">      
      <li class="nav-item">
        @if (Auth::guard('company')->user()) 
            <?php 
              $route = "company_logout";  
            ?>
        @else
            <?php 
              $route = "admin_logout";  
            ?>
        @endif
        <a class="nav-link" data-slide="true" href="{{ route($route) }}">
          Logout
        </a>
      </li>
    </ul>
</nav>