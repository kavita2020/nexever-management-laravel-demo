<div class="modal fade" id="employeeModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
        <form id="employeeForm" method="post">
            <input type="hidden" name="id" id="em_id">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">First name:</label>
            <input type="text" class="form-control" name="first_name" id="em_first_name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Last name:</label>
            <input type="text" class="form-control" name="last_name" id="em_last_name">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" id="em_email">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Password:</label>
            <input type="password" class="form-control" name="password" id="em_password">
          </div>
          <input type="hidden" name="company_id" id="em_company">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Phone:</label>
            <input type="text" class="form-control" name="phone" id="em_phone">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Salary:</label>
            <input type="text" class="form-control" name="salary" id="em_salary">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary saveData" data-model="employee">Save</button>
      </div>
    </div>
  </div>
</div>