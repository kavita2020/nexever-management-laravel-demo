<table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Logo</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="list_body">
        @foreach ($companies as $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td class="name" >{{ $value->name }}</td>
                <td class="email" >{{ $value->email }}</td>
                <td>
                    @if($value->logo)
                        <img class="logo_img" src="{{ asset('/storage').'/'.$value->logo }}">
                    @endif
                </td>
                <td>
                    <a class="btn btn-success  viewBtn" href="{{ route('company_show',['id' => $value->id ]) }}">Detail</a>
                    <button type="button" class="btn btn-primary mg-left editBtn" data-title="Edit Company" data-url="{{ route('company_update') }}" data-id="{{ $value->id }}" data-type="edit">Edit</button>
                    <button type="button" class="btn btn-danger mg-left deleteBtn" data-id="{{ $value->id }}" data-url="{{ route('company_destroy') }}">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="pagination">
    {!! $companies->render() !!}
</div>