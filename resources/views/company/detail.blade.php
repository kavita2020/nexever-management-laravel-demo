@extends('layouts.webLayout')

@section('title', 'Company Detail')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Company detail</h1>
            </div>
        </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">                
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-4">Company Name</dt>
                                <dd class="col-sm-8">{{ $company['name'] }}</dd>

                                <dt class="col-sm-4">Company Email</dt>
                                <dd class="col-sm-8">{{ $company['email'] }}</dd>
                            
                                <dt class="col-sm-4">Logo</dt>
                                <dd class="col-sm-8"><img class="detail_img" src="{{ asset('storage').'/'.$company['logo'] }}"></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('employee.data')
    
@endsection

@section('scripts')
    <script src="{{ asset('/js/employee.js') }}"></script>
@endsection