<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Authenticatable
{
    use HasFactory;

    protected $fillable = ['id', 'name', 'email', 'logo', 'password'];
    public $timestamps = false;

    protected $hidden = [
        'password',
    ];
}
