<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        $companies = Company::paginate(5);
        
        if ($request->ajax()) {

            return view('company.list',compact('companies'));
        
        }

       return view('company.index', compact('companies'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {
            $filename = "";

            if($request->hasFile('logo')){
                $image = $request->file('logo');
                $filename = 'logo_'.time() . '.' . $image->getClientOriginalExtension();
                $path = storage_path('/app/public/');
                $request->file('logo')->move($path, $filename);
            };

            $companyCreate = Company::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'logo' => $filename
            ]);
            
            if ($companyCreate) {

                return ['status' => 'success', 'message' => "Company added successfully", 200];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function show($id, Request $request)
    {
        $company = Company::where('id',$id)->first();
        
        $employee = User::where('company_id',$id)->paginate(5);

        if ($request->ajax()) {

            return view('employee.list',compact('employee'));
        
        }
        return view('company/detail')->with(['company' => $company, 'employee' => $employee]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|max:255|unique:companies,email,'.$request->id.',id'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {

            $logo = [];
            $updateData = [
                'name' => $request->name,
                'email' => $request->email,
            ];

            if ($request->hasFile('logo')) {
                $image = $request->file('logo');
                $filename = 'logo_'.time() . '.' . $image->getClientOriginalExtension();
                $path = storage_path('/app/public/');
                $request->file('logo')->move($path, $filename);
                $logo = ['logo' => $filename];
                
                $updateData = array_merge($updateData, $logo);

                $oldImage = Company::where('id', $request->id)->pluck('logo')->first();
                if ($oldImage) {
                    $oldImagePath = $path.'/'.$oldImage;
                    @unlink($oldImagePath);
                }
            };

            $updated = Company::where('id', $request->id)
                        ->update($updateData);

            return ['status' => 'success', 'message' => "Company has been updated successfully"];
        }
    }

    public function destroy(Request $request)
    {
        $data = $request->all();
        $status = "fail";
        $message = "Something went wrong.";

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {
            $deleted = Company::where('id',$data['id'])->delete();
            
            if ($deleted) {

                return ['status' => 'success', 'message' => "Company has been deleted successfully"];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }
}
