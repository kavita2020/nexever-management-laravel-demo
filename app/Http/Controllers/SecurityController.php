<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SecurityController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {

            return back()->with('error',$validator->errors()->first());

        } else {

            $status = 0;

            if ($request->login_user == "company_login" && auth()->guard('company')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
            {
                $status = 1;
                $user = auth()->guard('company')->user();
                
                \Session::put('success','You are Login successfully!!');

                return redirect()->route('employee_list',['companyId' => auth()->guard('company')->user()->id]);

            }

            if ($request->login_user == "admin_login" && auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
            {
                $status = 1;
                $user = auth()->guard('admin')->user();
                
                \Session::put('success','You are Login successfully!!');

                return redirect()->route('company');
                
            } 
            
            if ($status == 0) {
                return back()->with('error','your username and password are wrong.');
            }
        }
    }

    public function logout()
    {
        $routeName = \Request::route()->getName();
        
        if ($routeName == "admin_logout") {
            Auth::guard('admin')->logout();
            
            return redirect()->route('login_page');
        }

        if ($routeName == "company_logout") {
            Auth::guard('company')->logout();

            return redirect()->route('company_login_page');
        }
    }
}
