<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            
            return response()->json(['message' => $validator->errors()->first()], 400);
            
        } else {

            $email = Auth::guard('api')->user()->email;
            $password = Auth::guard('api')->user()->password;

            if ($email == $request->email && Hash::check($request->password, $password))
            {

                return response()->json(['message' => 'Logged in successully'], 200);

            } else {

                return response()->json(['message' => 'Invalid credentials'], 500);

            }
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' =>  'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required',
            'company_id' => 'required'
        ]);

        if ($validator->fails()) {
            
            return response()->json(['message' => $validator->errors()->first()], 400);
            
        } else {

            $insert['name'] = '';
            $insert['first_name'] = $request->first_name;
            $insert['last_name'] = $request->last_name;
            $insert['email'] = $request->email;
            $insert['password'] = Hash::make($request->password);
            $insert['company_id'] = $request->company_id;

            if (isset($request->phone)) {

                $insert['phone'] = $request->phone;
            }

            if (isset($request->salary)) {

                $insert['salary'] = $request->salary;
            }
            
            User::create($insert);

            return response()->json(['message' => 'User registered succesfully'], 200);

        }
    }
}
