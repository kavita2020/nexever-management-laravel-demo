<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EmployeeController extends Controller
{

    public function list(Request $request)
    {
        $employee = User::where('company_id',Auth::guard('company')->user()->id)->paginate(5);

        if ($request->ajax()) {

            return view('employee.list',compact('employee'));
        
        }

        return view('employee/index',['employee' => $employee, 'company' => Auth::guard('company')->user()]);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' =>  'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required',
            'company_id' => 'required'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {
            
            $employeeCreate = User::create([
                'name' => '',
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'salary' => $request->salary,
                'company_id' => $request->company_id,
                'password' => Hash::make($request->password)
            ]);
            
            if ($employeeCreate) {

                return ['status' => 'success', 'message' => "Employee added successfully", 200];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' =>  'required',
            'email' => 'required|max:255|unique:users,email,'.$request->id.',id'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {

            $updated = User::where('id',$request->id)
                        ->update([
                            'first_name' => $request->first_name,
                            'last_name' => $request->last_name,
                            'email' => $request->email,
                            'phone' => $request->phone,
                            'salary' => $request->salary,
                            'password' => Hash::make($request->password)
                        ]);
            
            if ($updated) {

                return ['status' => 'success', 'message' => "Employee has been updated successfully"];
            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }

    public function destroy(Request $request)
    {
        $data = $request->all();
        $status = "fail";
        $message = "Something went wrong.";

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {

            return ['status' => 'fail', 'message' => $validator->errors()->first()];

        } else {
            $deleted = User::where('id',$data['id'])->delete();
            
            if ($deleted) {

                return ['status' => 'success', 'message' => "Employee has been deleted successfully"];

            }

            return ['status' => 'fail', 'message' => "Some error occured, please try again"];
        }
    }
}
